package com.example.casinogames.util

import androidx.annotation.Keep

@Keep
data class CasinoGamesSplashResponse(val url : String)