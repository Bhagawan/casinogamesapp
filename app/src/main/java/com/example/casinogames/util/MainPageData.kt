package com.example.casinogames.util

import androidx.annotation.Keep

@Keep
data class MainPageData(val header: String, val text: String)
