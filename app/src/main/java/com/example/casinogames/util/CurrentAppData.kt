package com.example.casinogames.util

import androidx.compose.ui.graphics.Color
import kotlin.random.Random

object CurrentAppData {
    var url = ""
    var selectedGame: GameInfo? = null

    var mainPageData : List<MainPageData>? = null
    set(value) {
        selectedMain = value?.random()
        field = value
    }
    var gamesList : List<GameInfo>? = null
    set(value) {
        value?.let {
            selectedGames = if(it.size > 8) {
                it.shuffled().subList(0,8)
            } else it
        }
        field = value
    }

    var selectedGames : List<GameInfo> ? = null
    var selectedMain: MainPageData? = null

    var gradientTopColor = getRandomColor()
    var gradientBottomColor = getRandomColor()

    fun updateMain() {
        gradientTopColor = getRandomColor()
        gradientBottomColor = getRandomColor()
        selectedMain = mainPageData?.random()
        gamesList?.let {
            selectedGames = if(it.size > 8) {
                it.shuffled().subList(0,8)
            } else it
        }
    }

    private fun getRandomColor() = Color(Random.nextInt(255),Random.nextInt(255),Random.nextInt(255), 255)

    fun formatText(game: GameInfo): List<Pair<String, Boolean>> {
        val l = ArrayList<Pair<String, Boolean>>(game.description.split("\\n\\n".toRegex()).map { it to true })
        var nS = 0
        var n = 1
        while (n < l.size) {
            if(nS < game.screenshots.size) {
                l.add(n, game.screenshots[nS] to false)
                nS++
                n++
            }
            n++
        }
        while(nS < game.screenshots.size) {
            l.add(game.screenshots[nS] to false)
            nS++
        }
        return l.toList()
    }
}