package com.example.casinogames.util

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface CasinoGamesServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<CasinoGamesSplashResponse>

    @GET(UrlMainPageData)
    suspend fun getMainPageData() : Response<List<MainPageData>>

    @GET(UrlGamesList)
    suspend fun getGamesList() : Response<List<GameInfo>>

    companion object {
        fun create() : CasinoGamesServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(CasinoGamesServerClient::class.java)
        }
    }

}