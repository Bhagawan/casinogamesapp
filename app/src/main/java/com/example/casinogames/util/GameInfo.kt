package com.example.casinogames.util

import androidx.annotation.Keep

@Keep
data class GameInfo(
    val name: String,
    val logo: String,
    val link: String,
    val description: String,
    val screenshots: List<String>)
