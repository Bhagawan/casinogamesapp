package com.example.casinogames.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.casinogames.ui.screens.*
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, errorScreenAction : () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MAIN_SCREEN.label) {
            BackHandler(true) {}
            CurrentAppData.updateMain()
            MainScreen()
        }
        composable(Screens.SERVER_ERROR_SCREEN.label) {
            if (webView.onBackPressed()) exitProcess(0)
            ErrorScreen(errorScreenAction)
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            if(CurrentAppData.selectedGame != null) GameScreen(game = CurrentAppData.selectedGame!!)
            else MainScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}