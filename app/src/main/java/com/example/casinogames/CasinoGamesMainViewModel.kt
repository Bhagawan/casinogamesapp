package com.example.casinogames

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.casinogames.ui.screens.Screens
import com.example.casinogames.util.CasinoGamesServerClient
import com.example.casinogames.util.CurrentAppData
import com.example.casinogames.util.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class CasinoGamesMainViewModel: ViewModel() {
    private var request: Job? = null
    private var mainRequest: Job? = null
    private var gamesRequest: Job? = null
    private val server = CasinoGamesServerClient.create()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun reconnect() {
        viewModelScope.launch {
            getData()
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> getData()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                getData()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else getData()
                } else getData()
            }
        } catch (e: Exception) {
            getData()
        }
    }

    private fun getData() {
        mainRequest = viewModelScope.async {
            val mainData = server.getMainPageData()
            if(mainData.isSuccessful) {
                if(mainData.body() != null) {
                    CurrentAppData.mainPageData = mainData.body()
                    checkCompletion()
                } else switchToErrorScreen()
            } else switchToErrorScreen()
        }
        gamesRequest = viewModelScope.async {
            val gamesList = server.getGamesList()
            if(gamesList.isSuccessful) {
                if(gamesList.body() != null) {
                    CurrentAppData.gamesList = gamesList.body()
                    checkCompletion()
                } else switchToErrorScreen()
            } else switchToErrorScreen()
        }
    }

    private fun checkCompletion() {
        if(mainRequest != null && gamesRequest != null) {
            if(mainRequest!!.isCancelled || gamesRequest!!.isCancelled) switchToErrorScreen()
            else if(CurrentAppData.mainPageData != null && CurrentAppData.gamesList != null) switchToApp()
        }
    }

    private fun switchToErrorScreen() {
        viewModelScope.launch {
            Navigator.navigateTo(Screens.SERVER_ERROR_SCREEN)
        }
    }

    private fun switchToApp() {
        Navigator.navigateTo(Screens.MAIN_SCREEN)
    }
}