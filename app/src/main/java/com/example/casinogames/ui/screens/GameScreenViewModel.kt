package com.example.casinogames.ui.screens

import androidx.compose.foundation.ScrollState
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class GameScreenViewModel: ViewModel() {
    private val _scrollState = MutableStateFlow(ScrollState(0))
    val scrollState = _scrollState.asStateFlow()


    fun scrollTo(position: Int) {
        _scrollState.tryEmit(ScrollState(position))
    }
}