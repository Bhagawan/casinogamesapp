package com.example.casinogames.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.casinogames.util.GameInfo
import com.example.casinogames.util.PathToImage

@Composable
fun GameIcon(game: GameInfo, modifier: Modifier = Modifier, onClick: () -> Unit = {}) {
    val imageUrl = PathToImage.plus(game.logo)
    Box(modifier = modifier.fillMaxSize().background(Color.Transparent, RoundedCornerShape(10.dp)).clickable(
        MutableInteractionSource(), null) { onClick()

    }) {
        Image(rememberImagePainter(imageUrl), modifier = Modifier.fillMaxSize().clip(RoundedCornerShape(10.dp)), contentDescription = game.name, contentScale = ContentScale.Crop)
    }
}