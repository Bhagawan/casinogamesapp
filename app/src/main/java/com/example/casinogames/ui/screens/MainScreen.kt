package com.example.casinogames.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.casinogames.R
import com.example.casinogames.ui.composables.GameIcon
import com.example.casinogames.ui.theme.Orange
import com.example.casinogames.ui.theme.Red
import com.example.casinogames.util.CurrentAppData
import com.example.casinogames.util.Navigator

@Composable
fun MainScreen() {
    Column(modifier = Modifier
        .fillMaxSize()
        .verticalScroll(rememberScrollState())) {
        val gradient = Brush.verticalGradient(listOf(CurrentAppData.gradientTopColor, CurrentAppData.gradientBottomColor))
        Box(modifier = Modifier
            .background(gradient)
            .clickable { Navigator.navigateTo(Screens.MAIN_SCREEN) }
            .padding(10.dp), contentAlignment = Alignment.Center) {
            Text(text = stringResource(id = R.string.app_name), fontSize = 40.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .background(Color.Black)
            .height(100.dp))
        Column(modifier = Modifier
            .fillMaxWidth()
            .background(gradient)
            .padding(10.dp)) {
            val games = CurrentAppData.selectedGames ?: emptyList()
            LazyVerticalGrid(columns = GridCells.Fixed(2), contentPadding = PaddingValues(4.dp), modifier = Modifier.height((games.size * 52).dp)) {
                items(games) { game ->
                    GameIcon(game = game, modifier = Modifier
                        .size(100.dp)
                        .padding(4.dp)) {
                        CurrentAppData.selectedGame = game
                        Navigator.navigateTo(Screens.GAME_SCREEN)
                    }
                }
            }

            Text(text = CurrentAppData.selectedMain?.header ?: "",
                fontSize = 40.sp,
                color = Red,
                style = TextStyle(shadow = Shadow(Orange, offset = Offset(2.0f, 2.0f))),
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth())
            Text(text = CurrentAppData.selectedMain?.text ?: "",
                fontSize = 15.sp,
                color = Orange,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 20.dp))
        }
    }
}