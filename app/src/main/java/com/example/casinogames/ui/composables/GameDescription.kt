package com.example.casinogames.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.casinogames.R
import com.example.casinogames.ui.theme.Orange
import com.example.casinogames.ui.theme.Yellow
import com.example.casinogames.util.CurrentAppData
import com.example.casinogames.util.GameInfo
import com.example.casinogames.util.PathToImage

@Composable
fun GameDescription(game: GameInfo, modifier: Modifier = Modifier) {
    val text = CurrentAppData.formatText(game)
    Column(modifier = modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        Text(text = stringResource(id = R.string.game_desc_header),
            fontSize = 25.sp,
            color = Orange,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp))
        for(pair in text) {
            if(pair.second) Text(text = pair.first,
                fontSize = 15.sp,
                color = Yellow,
                textAlign = TextAlign.Start,
                modifier = Modifier.fillMaxWidth())
            else {
                Box(modifier = Modifier.fillMaxWidth().wrapContentHeight(), contentAlignment = Alignment.Center) {
                    Image(rememberImagePainter(PathToImage.plus(pair.first)),modifier = Modifier.fillMaxWidth().height(200.dp), contentDescription = "", contentScale = ContentScale.FillWidth)
                }
            }
        }
    }
}