package com.example.casinogames.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.casinogames.R
import com.example.casinogames.ui.theme.Orange
import com.example.casinogames.ui.theme.Red

@Composable
fun PlayButton(modifier: Modifier = Modifier, action: () -> Unit = {}) {
    BoxWithConstraints(modifier = modifier.clickable { action() }) {
        val brush = Brush.linearGradient(
            0.0f to Color(255, 228, 1, 255),
            0.1f to Color(255, 247, 194, 255),
            0.5f to Color(255, 228, 1, 255),
            0.9f to Color(255, 138, 0, 255),
            1.0f to Color(255, 228, 1, 255),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        Text(
            stringResource(id = R.string.btn_play),
            fontSize = 40.sp,
            color = Red,
            style = TextStyle(shadow = Shadow(Orange, offset = Offset(2.0f, 2.0f))),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth().background(brush, RoundedCornerShape(20.dp)))
    }

}