package com.example.casinogames.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.casinogames.R
import com.example.casinogames.ui.composables.GameDescription
import com.example.casinogames.ui.composables.GameIcon
import com.example.casinogames.ui.composables.PlayButton
import com.example.casinogames.util.CurrentAppData
import com.example.casinogames.util.GameInfo
import com.example.casinogames.util.Navigator

@Composable
fun GameScreen(game: GameInfo) {
    var currentGame by remember { mutableStateOf(game) }
    val viewModel = viewModel(GameScreenViewModel::class.java)
    val scrollState by viewModel.scrollState.collectAsState()
    Column(modifier = Modifier
        .fillMaxSize()
        .verticalScroll(scrollState)) {
        val gradient = Brush.verticalGradient(listOf(CurrentAppData.gradientTopColor, CurrentAppData.gradientBottomColor))
        Box(modifier = Modifier
            .background(gradient)
            .clickable { Navigator.navigateTo(Screens.MAIN_SCREEN) }
            .padding(10.dp), contentAlignment = Alignment.Center) {
            Text(text = stringResource(id = R.string.app_name), fontSize = 40.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
        }
        Box(modifier = Modifier
            .background(Color.Black)
            .fillMaxWidth()
            .height(100.dp))
        Column(modifier = Modifier
            .fillMaxWidth()
            .background(gradient)
            .padding(10.dp)) {
            Text(text = currentGame.name,
                fontSize = 40.sp,
                color = Color.White,
                textAlign = TextAlign.Start,
                modifier = Modifier.fillMaxWidth())
            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                GameIcon(game = currentGame, modifier = Modifier
                    .height(100.dp)
                    .wrapContentWidth())
            }
            PlayButton(modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 10.dp)) {
                CurrentAppData.url = currentGame.link
                Navigator.navigateTo(Screens.WEB_VIEW)
            }

            GameDescription(currentGame, modifier = Modifier.fillMaxWidth())
            Spacer(modifier = Modifier.height(20.dp))
            val games = CurrentAppData.selectedGames ?: emptyList()
            LazyVerticalGrid(columns = GridCells.Fixed(2), contentPadding = PaddingValues(4.dp), modifier = Modifier.height((games.size * 52).dp)) {
                items(games) { g ->
                    GameIcon(game = g, modifier = Modifier
                        .size(100.dp)
                        .padding(4.dp)) {
                        CurrentAppData.selectedGame = g
                        currentGame = g
                        viewModel.scrollTo(0)
                    }
                }
            }
        }
    }
}